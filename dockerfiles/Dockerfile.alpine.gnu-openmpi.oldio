# Dockerfile for SCHISM test case
#
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
# SPDX-License-Identifier: CC0-1.0
#
# Download base image alpine
FROM alpine:edge

LABEL maintainer="carsten.lemmen@hereon.de"
LABEL version="0.1a"
LABEL description="This is a custom Docker image for  a basic SCHISM \
  installation and test case"

# Install a development toolchain based on gnu/openmpi, git and  build tools
# Alpine/edge has gcc-10.3
RUN apk upgrade && apk add \
 cmake \
 git \
 gfortran g++  \
 netcdf \
 openmpi \
 make

# Define working directory, here root's home
WORKDIR /root

ENV SCHISM_DIR /root/devel/schism
RUN git clone --depth=1 https://github.com/schism-dev/schism.git ${SCHISM_DIR}
RUN ls ${SCHISM_DIR} ${SCHISM_DIR}/src
RUN cmake -S ${SCHISM_DIR}/src -B ${SCHISM_DIR}/build -DUSE_FABM=OFF -DOLDIO=ON

# @todo when running the container, only this last command is executed,
# so make sure to add the execution of a test case here
CMD ["make -C ${SCHISM_DIR}/build -j 8 pschism"]
