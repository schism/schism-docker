# Dockerfile for SCHISM development in a code-server (VSCode)
# browser-based development engine, based on
# https://github.com/linuxserver/docker-code-server
#
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
# SPDX-License-Identifier: CC0-1.0
#
# Download base image ubuntu/impish 21.10
FROM ubuntu:21.10

LABEL maintainer="carsten.lemmen@hereon.de"
LABEL version="0.1a"
LABEL description="This is a custom Docker image for a SCHISM \
  development environment"

# environment settings
ENV HOME="/config"

RUN \
  apt-get update && apt-get install -y gnupg curl

RUN \
  curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - && \
  echo 'deb https://deb.nodesource.com/node_14.x impish main' \
    > /etc/apt/sources.list.d/nodesource.list && \
#  curl -s https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
#  echo 'deb https://dl.yarnpkg.com/debian/ stable main' \
#    > /etc/apt/sources.list.d/yarn.list

RUN \
  apt-get update && apt-get install -y \
    build-essential \
    libx11-dev \
    libxkbfile-dev \
    libsecret-1-dev \
    pkg-config \
    git \
    jq \
    nano \
    net-tools \
    nodejs \
    sudo \
    yarn
RUN \
  if [ -z ${CODE_RELEASE+x} ]; then \
    CODE_RELEASE=$(curl -sX GET https://registry.yarnpkg.com/code-server \
    | jq -r '."dist-tags".latest' | sed 's|^|v|'); \
  fi && \
  CODE_VERSION=$(echo "$CODE_RELEASE" | awk '{print substr($1,2); }') && \
  yarn config set network-timeout 600000 -g && \
  yarn --production --verbose --frozen-lockfile global add code-server@"$CODE_VERSION" && \
  yarn cache clean
RUN apt-get update && apt-get install -y \
 cmake \
 git \
 gfortran g++ \
 libnetcdf-dev libnetcdff-dev \
 libmpich-dev \
 make \
 python3-minimal

RUN  \
  apt-get purge --auto-remove -y \
    libx11-dev \
    libxkbfile-dev \
    libsecret-1-dev \
    pkg-config && \
  apt-get clean && \
  rm -rf \
    /tmp/* \
    /var/lib/apt/lists/* \
    /var/tmp/*

# Define working directory, here schism user's home
RUN useradd -d /home/schism schism
USER schism

WORKDIR /home/schism
ENV SCHISM_DIR /home/schism/devel/schism
RUN git clone --depth=1 https://github.com/schism-dev/schism.git ${SCHISM_DIR}

# add local files
# COPY /root /

# ports and volumes
EXPOSE 8443
