<!--
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC0-1.0
-->

This is a collection of dockerfiles to test the build of SCHISM on various
platforms.

The dockerfiles are contained in the subdirectory `./dockerfiles`.  With a
local Docker daemon running, you can build the containers by issuing

```
docker build . -t <image-name> -f dockerfiles/<docker-file>
```

Please substitute `<image-name>` with a name of your choice and `<docker-file>`
with the filename in `dockerfiles` you'd like to test. You may need to run
with elevated privileges (`sudo`).

# List of dockerfiles

- `dockerfiles/Dockerfile.ubuntu-impish.gnu-mpich.oldio`: A linux ubuntu/impish
  (21.10) image with gnu/mpich toolchain and OLDIO option

- `dockerfiles/Dockerfile.ubuntu-focal.gnu-mpich.oldio`: A linux ubuntu/focal
  (20.04) image with gnu/mpich toolchain and OLDIO option

## Not working yet

- `dockerfiles/Dockerfile.homebrew.gnu-mpich.oldio`: A homebrew package manager
    image with gnu/mpich toolchain and OLDIO option

- `dockerfiles/Dockerfile.ubuntu-impish.gnu-mpich.oldio`: A linux alpine image
  with `apk` package manager, gnu/openmpi toolchain and OLDIO option.  Does not
  work due to missing development files for `netcdf` and `openmpi`

# Executing the container

Mount the local work directory to the container's work directory `~/work` and
create a container, named with the `--name` argument.  For a local work
directory `~/devel/schism/schism_verification_tests/Test_CORIE` and an image
named `schism-ubuntu`, the command should be:

```
docker run -v ~/devel/schism/schism_verification_tests/Test_CORIE:~/work
--name=schism -t schism-ubuntu /bin/bash
```
You can also provide the schism executable as the last argument to the `docker
run` command, or exit it interactively in the container.

```
docker run  -v ~devel/schism/schism_verification_tests/Test_CORIE:/home/schism/work --name=schism -t schism-ubuntu-20 /home/schism/devel/schism/build/bin/pschism_OLDIO_TVD-VL
```

## Interacting with the containers
```
docker run -it schism-ubuntu /bin/bash
```
